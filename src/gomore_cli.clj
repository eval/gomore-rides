(ns gomore-cli
  (:require   #_[nextjournal.clerk :as clerk]
              #_[nextjournal.clerk.viewer :as vr]
              [clojure.string :as str]
              [net.cgrand.enlive-html :as html]
              [tick.core :as t]))

(defn dom [url] (html/html-resource (java.net.URL. url)))

(defn ride-nodes [dom] (html/select dom [:div#rides :ul :> (html/but :.sticky)]))

(defn node-extractor
  ""
  [& {:as rules}]
  (fn [node]
    (reduce (fn [acc [att [sel transform]]]
              (assoc acc att (-> node (html/select sel) first transform))) {} rules)))

(defn extract-id [node]
  (last (re-find #"\/rides\/(.+)\?" (get-in node [:attrs :href]))))

;; turns date-string into date
(defn foo [s]
  (let [[date _ time]  (map str/trim (str/split-lines s))
        date-formatter (t/formatter "dd/M/yyyy")
        d              (condp re-matches (str/lower-case date)
                         #"i morgen" (t/tomorrow)
                         #"i dag"    (t/today)
                         (t/parse-date (str date "/2022") date-formatter))]
    (t/format :iso-instant (t/in (t/at d (t/time time)) "Europe/Amsterdam"))))


#_(t/format :iso-instant (foo "14/10\n          kl.\n          16:00"))

#_(str (t/tomorrow))


#_(t/parse-date "14/3/2022" (t/formatter "dd/M/yyyy"))
(defn search-url [date]
  (str "https://gomore.dk/rides?utf8=%E2%9C%93&within=10&filter_search=&baggage_size=&from=&from_latitude=&from_longitude=&to=&to_latitude=&to_longitude=&on=" date))

(defn ^#:inclined{:option.date     {}
                  :option.dev?     {:default false}
                  :option.confirm? {:desc    "Show preview and confirm before posting"
                                    :default true}}
  search
  [{:keys [date]}]
  {:pre [(seq date)]}
  ;; date e.g. "2022-03-12T12:00"
  ;; dan items die

  ;; je piped output:
  ;; cli search --date '' >> rides.html
  ;; alleen het rides-html deel outputten
  ;; in output moet alle 'i dag' en 'i morgen' worden vervangen door echte data
  (let [url   (search-url date)
        nodes (ride-nodes (dom url))]
    (dorun (map
            (comp prn (node-extractor :from [[:div.mb3 :h4.mb0.text-semi-bold html/text] str/trim]
                                      :to   [[:div.mb0 :h4.mb0.text-semi-bold html/text] str/trim]
                                      :id [[:a] extract-id]
                             :at   [[:h3.f3 html/text] (comp foo str/trim) #_(comp str/trim last str/split-lines str/trim)]))
      nodes))))


(comment
  ;; I morgen

  ;; parse date:
  ;; i morgen ->
  ;; 14/3
  (t/at (t/tomorrow) (t/time "13:00"))
  (t/today)
  (t/in (t/at (t/date "01/01/2018") (t/time "13:00")) "Europe/Amsterdam")
  (t/zoned-date-time "2017-12-08T12:00:10+01:00[Europe/Amsterdam]")
  (t/parse-zoned-date-time "14/3/2022+01:00 16:00" #_(t/formatter "dd/M/yyyy HH:mm"))

  (.day (java.util.Date.))
  (new java.util.Date)
  (t/format *1 (t/zoned-date-time (java.util.Date.)))
  (str (t/format :iso-local-date (str (t/now))))
  (def nodes (ride-nodes (dom (search-url "2022-03-13T11:00"))))
  (dorun
   (map (comp prn (node-extractor :from [[:div.mb3 :h4.mb0.text-semi-bold html/text] str/trim]
                                  :to   [[:div.mb0 :h4.mb0.text-semi-bold html/text] str/trim]
                                  :id   [[:a] extract-id]
                                  :at   [[:h3.f3 html/text] (comp foo str/trim) #_(comp str/trim last str/split-lines str/trim)]))
        nodes))

  (html/select (first nodes) [:a #(html/attr-values % :href)])
  html/attr?
  (def debug (atom nil))
  ((node-extractor :from [[:div.mb3 :h4.mb0.text-semi-bold html/text] str/trim]
                   :to   [[:div.mb0 :h4.mb0.text-semi-bold html/text] str/trim]
                   :id [[:a] #(reset! debug %)]
                   :at   [[:h3.f3 html/text] (comp str/trim last str/split-lines str/trim)]) (nth nodes 29))
  (re-find #"\/rides\/(.+)\?" (get-in @debug [:attrs :href]))
  (count nodes)

  (first nodes)
  )
